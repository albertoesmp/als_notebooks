## Virtual environment
To create virtual environment
```
mkdir -p <path>
cd <path>
virtualenv .
```


To load virtual environment
```
. bin/activate
```


## Requirements
To install requirements
```
pip install -r requirements.txt
```

Alternatively, if multiple pip for python2 and python3 are installed, the following is recommended to install requirements:
```
pip3 install -r requirements.txt
```


## Using jupyter with virtual environment
If **virtual environment is loaded**, it is possible to use it as context for jupyter-notebook as long as ipykernel is installed.

In case ipykernel must be installed:
```
pip install --user ipykernel
```

Alternatively, if there different pip installations for different python versions:
```
pip3 install --user ipykernel
```

Now, the virtual environment must be added to jupyter:
```
python -m ipykernel install --user --name=myenv
```

If everything is okay, following message will be reported:
```
Installed kernelspec myenv in /home/user/.local/share/jupyter/kernels/myenv
```


